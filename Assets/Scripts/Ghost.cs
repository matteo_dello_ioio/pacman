﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    static public int Points = 200;

    Renderer _ghostMeshRenderer;
    GameManager _gameManager;
    NavMeshAgent _navAgent;
    GameObject _player;
    PacManMovement _pacman;

    [SerializeField]
    GameObject GhostMesh;

    [SerializeField]
    GameObject EyesMesh;

    [SerializeField]
    Transform SpawnPoint;

    bool _isDead;

    Collider _collider;

    [SerializeField]
    Material DefaultMaterial;

    [SerializeField]
    Material AfraidMaterial;




    void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _collider = gameObject.GetComponent<Collider>();
        _navAgent = GetComponent<NavMeshAgent>();
        _navAgent.updateRotation = false;

        _ghostMeshRenderer = GhostMesh.GetComponent<Renderer>();
        DefaultMaterial = _ghostMeshRenderer.material;
    }

    //void OnDisable()
    //{
    //    Destroy(GhostMesh);
    //}

    // Use this for initialization
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player == null)
            throw new UnityException("Player is missing");

        if (SpawnPoint == null)
            throw new UnityException("spawn point is missing");

        _pacman = _player.GetComponent<PacManMovement>();
        EyesMesh.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.IsGameReady() == false)
            return;

        //GhostMesh.transform.position = transform.position;
        EyesMesh.transform.position = transform.position;

        if (!_isDead) // ghost alive
        {
            _navAgent.SetDestination(_player.transform.position);

            SetMaterial();
        }
        else // ghost dead
        {
            if(IsInsideSpawnZone())
            {
                // respawn
                Respawn();
            }
        }
    }

    private void Respawn()
    {
        GhostMesh.SetActive(true);
        EyesMesh.SetActive(false);
        _isDead = false;
        _collider.enabled = true;

        SetMaterial();
    }

    private void SetMaterial()
    {
        if (_pacman.HasPowerUp())
        {
            _ghostMeshRenderer.material = AfraidMaterial;
        }
        else
        {
            _ghostMeshRenderer.material = DefaultMaterial;
        }
    }

    bool IsInsideSpawnZone()
    {
        return (gameObject.transform.position - SpawnPoint.transform.position).magnitude <= 0.5;
    }

    public void OnDeath()
    {
        _isDead = true;
        GhostMesh.SetActive(false);
        EyesMesh.SetActive(true);
        _navAgent.SetDestination(SpawnPoint.position);
        _collider.enabled = false;
    }
}
