﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManMovement : MonoBehaviour
{
    GameManager _gameManager;

    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioMov;

    [SerializeField]
    AudioClip _audioDeath;

    [SerializeField]
    float MovementSpeed;

    int _totalPoints = 0;

    public int GetTotalPoints()
    {
        return _totalPoints;
    }

    int Lives = 3;

    public int GetLives()
    {
        return Lives;
    }

    bool _hasPowerUp;

    public bool HasPowerUp()
    {
        return _hasPowerUp;
    }

    /// <summary>
    /// Tempo trascorso da quando si è raccolto il power-up.
    /// </summary>
    float _powerUpElapsedTime = 0;


    /// <summary>
    /// Indica la durata dell'ultimo power-up raccolto.
    /// </summary>
    float _powerUpDuration = 10;

    /// <summary>
    /// Rappresenta il numero di fantasmi mangiati durante l'effetto del power-up.
    /// </summary>
    int _eatenGhost;

    /// <summary>
    /// Rappresenta il numero di pillole mangiate da PacMan.
    /// </summary>
    int _eatenPills;

    public int GetEatenPills()
    {
        return _eatenPills;
    }




    void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.IsGameReady() == false)
            return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        //Debug.Log("h: " + h + " - v: " + v);
        if (h != 0)
        {
            transform.Translate(Vector3.right * MovementSpeed * h);
        }
        else
        {
            transform.Translate(Vector3.forward * MovementSpeed * v);
        }

        if (h != 0 || v != 0)
        {
            if (!_audioSource.isPlaying)
            {
                _audioSource.PlayOneShot(_audioMov);
            }
        }


        if(_hasPowerUp)
        {
            _powerUpElapsedTime += Time.deltaTime;

            if(_powerUpElapsedTime >= _powerUpDuration)
            {
                _hasPowerUp = false;
                _eatenGhost = 0;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "pill")
        {
            OnEatPill(other);
        }

        if (other.gameObject.tag == "ghost")
        {
            if (!_hasPowerUp)
            {
                OnHit();
            }
            else
            {
                OnEatGhost(other);
            }
        }
    }

    /// <summary>
    /// Codice eseguito quando PacMan mangia un fantasma.
    /// </summary>
    /// <param name="other"></param>
    private void OnEatGhost(Collider other)
    {
        if (other == null)
            throw new System.ArgumentNullException("other", "other cannot be null");

        _eatenGhost++;

        _totalPoints += (int)Mathf.Pow(2, _eatenGhost - 1) * Ghost.Points;
        //Destroy(other.gameObject);
        other.gameObject.SendMessage("OnDeath", SendMessageOptions.RequireReceiver);
    }

    /// <summary>
    /// Codice eseguito quando si mangia la pillola.
    /// </summary>
    /// <param name="other"></param>
    void OnEatPill(Collider other)
    {
        Debug.Log("GNAM!");

        _eatenPills++;

        Pill pill = other.gameObject.GetComponent<Pill>();
        _totalPoints += pill.Points;

        if (pill is PowerUp)
        {
            Debug.Log("POWER UP!!!");
            _hasPowerUp = true;
            _powerUpElapsedTime = 0;
        }

        Destroy(other.gameObject);
    }

    /// <summary>
    /// Codice eseguito quando PacMan viene colpito da un fantasma.
    /// </summary>
    void OnHit()
    {
        Debug.Log("GAME OVER");
        if (_audioSource.isPlaying)
        {
            _audioSource.Stop();
        }
        _audioSource.PlayOneShot(_audioDeath);

        Lives -= 1;
    }

}